﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4_Example
{
    class Student : Person
    {
        public void TakesTheBus()
        {
            Console.WriteLine($"{ChangeName} now go take the bus with this programming logic.");
        }
    }
    
}

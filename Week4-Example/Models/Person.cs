﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week4_Example
{
    public class Person
    {
        string Name;
        string Age;
        string DOB;

        public string ChangeName
        {
            get
            {
                var greeting = $"Hello";
                return greeting + Name;
            }

            set { Name = value; }
        }

        public void GoShopping()
        {
            Console.WriteLine($"This is the code that tells {Name} to go shopping.");
        }
    }
}
